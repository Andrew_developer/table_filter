<?php

namespace core;

use \core\File;

class Doc extends File
{
    public function __construct()
    {
        var_dump('doc');
    }

    public function handler($filename){
        $this->setFile($filename);

        if(($fh = fopen($this->getFile(), 'r')) !== false ) {
            $contents_doc = fread($fh, filesize($this->getFile()));
            fclose($fh);
            echo $contents_doc;
        }
    }
}
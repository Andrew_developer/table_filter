<?php

namespace core;

use core\Interfaces\InterfaceFileFactory;

class FileFactory implements InterfaceFileFactory
{
    public function createDoc()
    {
        return new Doc();
    }

    public function createXml()
    {
        return new Xml();
    }

    public function createTxt()
    {
        return new Txt();
    }
}
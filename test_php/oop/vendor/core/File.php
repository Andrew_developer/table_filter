<?php

namespace core;

use \core\Interfaces\InterfaceFile;

class File implements InterfaceFile
{
    private $file = null;

    public function setFile($file){
        $this->file = $file;
    }

    public function getFile(){
        return $this->file;
    }
}
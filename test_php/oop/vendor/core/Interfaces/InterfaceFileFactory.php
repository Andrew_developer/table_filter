<?php

namespace core\Interfaces;

Interface InterfaceFileFactory{
    public function createDoc();

    public function createXml();

    public function createTxt();
}
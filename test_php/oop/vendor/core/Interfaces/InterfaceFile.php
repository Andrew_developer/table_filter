<?php

namespace core\Interfaces;

Interface InterfaceFile{
    public function setFile($file);

    public function getFile();
}
<?php

namespace core\base;

use \core\FileFactory;

class App
{
    public function __construct()
    {
        $type = 'doc';
        $fileFactory = new FileFactory();

        switch ($type) {
            case 'doc':
                $fileFactory->createDoc();
                break;
            case 'xml':
                $fileFactory->createXml();
                break;
            case 'txt':
                $fileFactory->createTxt();
                break;
        }
    }
}
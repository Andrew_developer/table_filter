<?php

define("root_dir", dirname(__dir__));
define("app_dir", root_dir . '/app');

require_once root_dir . '/vendor/autoload.php';
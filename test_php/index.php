<?php

//this is sql task
class DataBase
{
    protected $db = null;
    private $table = 'users';

    public function __construct()
    {
        $host = '127.0.0.1';
        $db = 'test';
        $user = 'root';
        $pass = '';
        $charset = 'utf8';

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            $this->db = new PDO($dsn, $user, $pass, $options);
        } catch (PDOException $e) {
            die('Connection fail: ' . $e->getMessage());
        }
    }

    public function getAll()
    {
        $sth = $this->db->prepare("SELECT *, COUNT(login) FROM $this->table GROUP BY login HAVING COUNT(login) > 1 ");
        $sth->execute();

        $result = $sth->fetchAll();

        var_dump($result);

        return $result;
    }
}

$db = new DataBase();
$db->getAll();